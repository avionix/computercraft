-- Inertial Navigation System
-- A dead-reckoning wrapper API for turtle movement

LOGPATH = '/api/logs/INS_log.json'

turtlePosition = vector.new(0, 0, 0)
turtleHeading = 'N'

local Compass = { 'N', 'E', 'S', 'W' }

local function compassShift(current, nextprev)
	-- Implements array cycling for compass headings
	for key,value in pairs(Compass) do
		local point = key + nextprev
		if value == current then
			if point == 4 or point == 0 then
				return Compass[4]
			end
			r = Compass[point % 4]
			return r
		elseif key == current then
			if point == 4 or point == 0 then
				return point
			end
		r = point % 4
		return r
		end
	end
end

local function headingNext(current)
	-- Shifts compass heading right
	return compassShift(current, 1)
end

local function headingPrev(current)
	-- Shifts compass heading left
	return compassShift(current, -1)
end

function headingReverse(current)
	-- Returns opposite compass heading
	return compassShift(current, 2)
end

local function translateOneMeter(heading)
	-- Updates position by one meter in direction
	if heading == 'N' then
		turtlePosition = turtlePosition:sub(vector.new(0, 0, 1))
	elseif heading == 'E' then
		turtlePosition = turtlePosition:add(vector.new(1, 0, 0))
	elseif heading == 'S' then
		turtlePosition = turtlePosition:add(vector.new(0, 0, 1))
	elseif heading == 'W' then
		turtlePosition = turtlePosition:sub(vector.new(1, 0, 0))
	elseif heading == 'U' then
		turtlePosition = turtlePosition:add(vector.new(0, 1, 0))
	elseif heading == 'D' then
		turtlePosition = turtlePosition:sub(vector.new(0, 1, 0))
	else
		error('Vector update failed.')
	end
end
--
-- Movement
function moveForward(distance)
	local distance = distance or 1
	for i = 1,distance do
		local success = turtle.forward()
		if success then
			local heading = turtleHeading
			translateOneMeter(heading)
		else
			return success
		end
	end
end

function moveBack(distance)
	local distance = distance or 1
	for i = 1,distance do
		local success = turtle.back()
		if success then
			local heading = headingReverse(turtleHeading)
			translateOneMeter(heading)
		else
			return success
		end
	end
end

function moveUp(distance)
	local distance = distance or 1
	for i = 1,distance do
		local success = turtle.up()
		if success then
			local heading = 'U'
			translateOneMeter(heading)
		else
			return success
		end
	end
end

function moveDown(distance)
	local distance = distance or 1
	for i = 1,distance do
		local success = turtle.down()
		if success then
			local heading = 'D'
			translateOneMeter(heading)
		else
			return success
		end
	end
end
--
-- Turning
function turnRight(times)
	local times = times or 1
	for i = 1,times do
		local success = turtle.turnRight()
		if success then
			turtleHeading = headingNext(turtleHeading)
		else
			return success
		end
	end
	return turtleHeading
end

function turnLeft(times)
	local times = times or 1
	for i = 1,times do
		local success = turtle.turnLeft()
		if success then
			turtleHeading = headingPrev(turtleHeading)
		else
			return success
		end
	end
	return turtleHeading
end

function faceHeading(aim)
	local result = turtleHeading
	local count = 1

	while result ~= aim do
		result = headingNext(result)
		count = count + 1
	end
	if count > 2 then
		while turtleHeading ~= aim do turnLeft() end
	else
		while turtleHeading ~= aim do turnRight() end
	end
	return true
end

function moveToPosition(destination, destructive, order)
	-- Travels to an XYZ point (vector). order is array of axis priorities.
	-- destructive is bool of whether to dig blocks in the way.
	local displacement = destination - turtlePosition
	local order = order or { 'y', 'x', 'z'}
	local destructive = destructive or false

	local function digIfBlocked(destroy)
		if turtle.detect() then turtle.dig() end
	end

	local function digUpIfBlocked()
		if turtle.detectUp() then turtle.digUp() end
	end

	local function digDownIfBlocked()
		if turtle.detectDown() then turtle.digDown() end
	end

	for key, axis in pairs(order) do
		if axis == 'y' then
			if displacement.y < 0 then
				while displacement.y ~= 0 do
					if destructive then digDownIfBlocked() end
					moveDown()
					displacement = destination - turtlePosition
				end
			elseif displacement.y > 0 then
				while displacement.y ~= 0 do
					if destructive then digUpIfBlocked() end
					moveUp()
					displacement = destination - turtlePosition
				end
			end

		elseif axis == 'x' then
			if displacement.x < 0 then faceHeading('W')
			elseif displacement.x > 0 then faceHeading('E')
			end

			while displacement.x ~= 0 do
				if destructive then digIfBlocked() end
				moveForward()
				displacement = destination - turtlePosition
			end

		elseif axis == 'z' then
			if displacement.z < 0 then faceHeading('N')
			elseif displacement.z > 0 then faceHeading('S')
			end

			while displacement.z ~= 0 do
				if destructive then digIfBlocked() end
				moveForward()
				displacement = destination - turtlePosition
			end
		end
	end
end

