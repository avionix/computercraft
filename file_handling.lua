local function readFile(path)
	if fs.exists(path) then
		local handle = io.open(path, 'r')
		local fileString = file:read()
		file:close()
		return textutils.unserialize(fileString)
	end
end

local function writeFile(path, content)
	local handle = io.open(path, 'w')
	io.output(handle)
	io.write(content)
	handle:flush()
	handle:close()
end


local function serializePosition()
	-- Returns a serialized string from pos. data
	data = { position, heading }
	return textutils.serialize(data)
end