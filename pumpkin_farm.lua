require('/api/INS')

REPEAT = true
WAIT_TIME = 900

function stepForward(distance)
	local distance = distance or 1
	for d = 1,distance do
		if turtle.detect() then
			turtle.dig()
		end
		moveForward()
	end
end

function climbRight()
	turnRight()
	stepForward()
	turnRight()
	moveUp(2)
end

function climbLeft()
	turnLeft()
	stepForward()
	turnLeft()
	moveUp(2)
end

function dumpAllItemsForward()
	for f = 1,16 do
		turtle.select(f)
		turtle.drop()
	end
end

function findEmptySlot()
	for s = 1,16 do
		if turtle.getItemCount(s) == 0 then
			return s
		end
	end
end

function main()
	-- First row
	moveForward(2)
	turnLeft()
	stepForward(14)

	-- Second row
	turnLeft()
	moveUp(2)
	stepForward()
	turnLeft()
	stepForward(14)

	-- Third row
	turnRight()
	moveUp(2)
	stepForward()
	turnRight()
	stepForward(14)

	-- Return to base
	turnRight(2)
	stepForward(14)
	turnRight()
	moveDown(3)

	-- Refuel if necessary
	if turtle.getFuelLevel() <= 500 then
		local slot = findEmptySlot()
		local fuelItemsUsed = 0
		while turtle.getFuelLevel() <= 4500 do
			turtle.select(slot)
			turtle.suck(1)
			turtle.refuel()
			local fuelItemsUsed = fuelItemsUsed + 1
		end
		print('Turtle refueled with', fuelItemsUsed, 'fuel items.')
	end

	moveDown()

	-- Dump harvest in chest
	dumpAllItemsForward()
	turnLeft(2)
end

-- Begin main program

if REPEAT == true then
	while true do
		main()
		print('Sleeping for', WAIT_TIME, 'seconds...')
		os.sleep(WAIT_TIME)
	end
else
	main()
end
