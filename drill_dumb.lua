-- Drills a simple rectangle. Start at bottom left facing up.

require('/api/INS')

TARGET_DIMENSIONS = vector.new(arg[1], arg[2], arg[3])

ROW_DIRECTION = { 'RIGHT', 'LEFT' }

nextRowDirection = 1

function nextElementInArray(array, currentIndex)
	-- Loops back to beginning if at end
	if currentIndex < #array then
		return currentIndex + 1
	else
		return 1
	end
end

function digMoveForward()
	if turtle.detect() then
		turtle.dig()
	end
	moveForward()
end

function turnAroundForNextRow()
	-- Indexes along X axis and turns around
	if ROW_DIRECTION[nextRowDirection] == 'LEFT' then
		turnLeft()
		digMoveForward()
		turnLeft()
	elseif ROW_DIRECTION[nextRowDirection] == 'RIGHT' then
		turnRight()
		digMoveForward()
		turnRight()
	end
	nextRowDirection = nextElementInArray(ROW_DIRECTION, nextRowDirection)
end

function descendForNextLayer()
	turnLeft(2)
	if turtle.detectDown() then
		turtle.digDown()
	end
	moveDown()
end

-- Digging loop

for z = 1, TARGET_DIMENSIONS.z do
	for x = 1, TARGET_DIMENSIONS.x do
		for y = 1, TARGET_DIMENSIONS.y-1 do
			digMoveForward()
		end
		if x < TARGET_DIMENSIONS.x then turnAroundForNextRow() end
	end
	if z < TARGET_DIMENSIONS.z then descendForNextLayer() end
end

turnLeft(2)


