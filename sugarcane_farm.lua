require('/api/INS')

REPEAT = true
WAIT_TIME = 2600
PASSES = 2

function suckDryDown()
	while turtle.suckDown() do
	end
end

function suckDryUp()
	while turtle.suckUp() do
	end
end

function stepForward(distance)
	local distance = distance or 1
	for d = 1,distance do
		if turtle.detect() then
			turtle.dig()
		end
		moveForward()
		suckDryUp()
		suckDryDown()
	end
end

function turnAroundRight(space)
	local space = space or 1
	turnRight()
	stepForward(space)
	turnRight()
end

function turnAroundLeft(space)
	local space = space or 1
	turnLeft()
	stepForward(space)
	turnLeft()
end

function dumpAllItemsForward()
	for f = 1,16 do
		turtle.select(f)
		turtle.drop()
	end
end

function findEmptySlot()
	for s = 1,16 do
		if turtle.getItemCount(s) == 0 then
			return s
		end
	end
end

function main()
	print('Beginning harvest.')

	stepForward() -- Leave base

	for l = 1,PASSES do -- Multiple passes to pick up missed items
		print('Start pass', l, 'of', PASSES, '.')
		stepForward(16)
		turnAroundRight(2)

		for i = 1,3 do
			stepForward(16)
			turnAroundLeft(1)

			stepForward(16)
			turnAroundRight(2)
		end
		stepForward(16)

		turnRight()
		stepForward(11) -- Travel to shaft
		if l ~= PASSES then
			turnRight()
		end
	end

	print('Collecting drops from hoppers.')
	-- Leave farm
	turnLeft()
	moveForward(2) -- Now out of farm surface
	turnLeft()
	moveForward()
	turnLeft()

	-- Descend shaft
	moveDown(4)
	moveForward(9)

	-- Begin loop of hoppers
	turnRight()
	moveForward(9)
	turnLeft()
	moveForward()
	turnLeft()

	-- Suck hoppers
	for i = 1,3 do
		suckDryUp()
		moveForward(3)
	end
	suckDryUp()

	-- Travel to elevator and climb
	turnLeft()
	moveForward(10)
	moveUp(4)

	-- Refuel if necessary
	if turtle.getFuelLevel() <= 500 then
		local slot = findEmptySlot()
		local fuelItemsUsed = 0
		while turtle.getFuelLevel() <= 4500 do
			turtle.select(slot)
			turtle.suck(1)
			turtle.refuel()
			local fuelItemsUsed = fuelItemsUsed + 1
		end
		print('Turtle refueled with', fuelItemsUsed, 'fuel items.')
	end

	-- Empty inventory into chest
	turnRight()
	moveForward()
	turnLeft()
	dumpAllItemsForward()
	print('Harvest deposited in chest.')

	-- Return to base block
	turnRight(2)
	moveForward()
	print('Harvest complete.')
end

-- Begin main program

if REPEAT == true then
	while true do
		main()
		print('Sleeping for', WAIT_TIME, 'seconds...')
		os.sleep(WAIT_TIME)
	end
else
	main()
end
