function cruise(direction, dist)
    if direction == 'F' then
        for i = 1,dist do
            turtle.forward()
        end
    elseif direction == 'B' then
        for i = 1,dist do
            turtle.back()
        end
    elseif direction == 'U' then
        for i = 1,dist do
            turtle.up()
        end
    elseif direction == 'D' then
        for i = 1,dist do
            turtle.down()
        end
    end
end

for i = 1,2 do
    cruise('F', 14)
    turtle.turnLeft()
    cruise('F', 18)
    turtle.turnLeft()
end