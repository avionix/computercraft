
require('/api/INS')

SAPLING_SLOTS = {9,10}
SWEEP_WAIT_TIME = 320

x_size = 14
z_size = 18
margin = 1

endCoords = vector.new(-1, 1, 18)

-- Update INS location
turtleHeading = 'W'

function isEven(number)
	if number % 2 == 0 then
		return true
	else
		return false
	end
end

function digIfBlocked()
	if turtle.detect() then
		turtle.dig()
	end
end

function suckDryDown()
	while turtle.suckDown() do
	end
end

function suckDryUp()
	while turtle.suckUp() do
	end
end

function dumpAllItemsDown()
	for f = 1,16 do
		turtle.select(f)
		turtle.dropDown()
	end
end

function findEmptySlot()
	for s = 1,16 do
		if turtle.getItemCount(s) == 0 then
			return s
		end
	end
end

function forwardIsTree()
	if turtle.detect() then
		turtle.dig()
		moveForward()
		if turtle.detectDown() and
				turtle.detectUp() and
				isEven(turtlePosition.x) and
				isEven(turtlePosition.z) then
			return 'tree'
		end
	else
		moveForward()
		if turtle.detectDown() and
			not turtle.detectUp() and
				isEven(turtlePosition.x) and
				isEven(turtlePosition.z) then
			print('Sapling detected.')
			return 'sapling'
		else
			return 'nothing'
		end
	end
end

function harvestTree()
	local height = 0
	if turtle.detectDown() then
		turtle.digDown()
	end
	while turtle.detectUp() do
		turtle.digUp()
		moveUp()
		height = height + 1
	end
	for i = 1,height do
		if turtle.detectDown() then
			turtle.digDown()
		end
		moveDown()
	end
end

function plantTree()
	for key, value in pairs(SAPLING_SLOTS) do
		if turtle.getSelectedSlot() ~= value then
			turtle.select(value)
			local itemData = turtle.getItemDetail()

			if turtle.getItemCount() > 0 then
				if string.find(itemData.name, 'sapling') then
					turtle.placeDown()
					return true
				end
			end
		end
	end
end

function step()
	isTree = forwardIsTree()
	if isTree == 'tree' then
		harvestTree()
		suckDryDown()
		return 'Harvested tree'
	end
	suckDryDown()
	return isTree
end

function moveToPosition(destination, order)
	-- Travels to an XYZ point. Order indicates the order to move axes.
	displacement = destination - turtlePosition
	order = order or { 'y', 'x', 'z'}

	for key, o in pairs(order) do
		print(o, ' Displacement', '=', displacement)
		if o == 'y' then
			-- Y axis
			if displacement.y < 0 then
				while displacement.y ~= 0 do
					if turtle.detectDown() then
						turtle.digDown()
					end
					moveDown()
					displacement = destination - turtlePosition
				end
			elseif displacement.y > 0 then
				while displacement.y ~= 0 do
					if turtle.detectUp() then
						turtle.digUp()
					end
					moveUp()
					displacement = destination - turtlePosition
				end
			end

		elseif o == 'x' then
			-- X axis
			if displacement.x < 0 then
				faceHeading('W')
			elseif displacement.x > 0 then
				faceHeading('E')
			end

			while displacement.x ~= 0 do
				digIfBlocked()
				moveForward()
				displacement = destination - turtlePosition
			end

		elseif o == 'z' then
			-- Z axis
			if displacement.z < 0 then
				faceHeading('N')
			elseif displacement.z > 0 then
				faceHeading('S')
			end

			while displacement.z ~= 0 do
				digIfBlocked()
				moveForward()
				displacement = destination - turtlePosition
			end
		end
	end
end

function moveToAisle()
	-- Travels to an empty aisle in the farm
	print('Moving to aisle.')
	if isEven(turtlePosition.x) then
		turnLeft(2)
		if turtle.detect() then
					turtle.dig()
		end
		moveForward()
	end
	if isEven(turtlePosition.z) then
		faceHeading('N')
		if turtle.detect() then
					turtle.dig()
		end
		moveForward()
	end
end

function sweepSaplings(start, startHeading, rows)
	-- Travels to `start` and vacuums up saplings before returning.
	local leftOff = {}
	leftOff.pos = turtlePosition
	leftOff.head = turtleHeading
	local rows = rows or false

	print('Initializing sapling sweep...')
	moveToAisle()

	-- Offset so that it will use the aisles
	local offset = vector.new(0, 0, 0)
	if isEven(start.x) then
		offset.x = 1
	end
	if isEven(start.z) then
		offset.z = 1
	end
	
	print('Moving to offset')
	moveToPosition(start + offset)
	print('Moving to non-offset position')
	moveToPosition(start)
	faceHeading(startHeading)
	-- Warns on the row before it is done sweeping
	local warnPosition = leftOff.pos + vector.new(0, 0, 1)

	local count = 0
	print('Beginning sweep.')
	while true do
		if turtlePosition.z == warnPosition.z or
					(rows ~= false and count >= rows) then
			moveToPosition(leftOff.pos, { 'y', 'x', 'z'})
			print('moveToPosition', moveToPosition)
			faceHeading(leftOff.head)
			print('faceHeading', faceHeading)
			-- Returns 3 variables
			print('Sapling sweep complete.')
			return turtlePosition, turtleHeading, os.clock()+SWEEP_WAIT_TIME
		end


		print('turtlePosition.x: ', turtlePosition.x)
		if (turtlePosition.x == -1 or turtlePosition.x == -17) and justTurned == false then
			-- Move to next row
			local tempHeading = turtleHeading
			print('Turning to ', headingReverse(tempHeading))
			faceHeading('S')
			step()
			faceHeading(headingReverse(tempHeading))
			justTurned = true

		else
			if step() ~= 'sapling'
					and isEven(turtlePosition.x)
					and isEven(turtlePosition.z) then
				plantTree()
			end
			justTurned = false
		end
		local count = count + 1
	end
end

function moveToOrigin()
	-- Travels to the local origin
	moveToAisle()
	moveToPosition(vector.new(0, 0, 0), { 'z', 'x', 'y' })
	faceHeading('W')
end

function main()
	-- Main farm cycle
	print('Beginning harvest.')
	if turtle.detectDown() then
		print('Moving to Y = 1.')
		moveUp()
	end
	moveForward()

	local sweepTimer = os.clock() + SWEEP_WAIT_TIME
	local sweepPosition = turtlePosition - vector.new(0, 0, 1)
	local sweepHeading = turtleHeading
	local turnDirection = 'L'
	rowCounter = 0

	while true do
		rowCounter = rowCounter + 1
		for i = 1,15 do
			if os.clock() >= sweepTimer then
				sweepPosition, sweepHeading, sweepTimer = sweepSaplings(
						sweepPosition, sweepHeading, 5)
			end
			step()
		end
		print('Row ', rowCounter, ' complete.')
		print('XYZH = ', turtlePosition.x, turtlePosition.y, turtlePosition.z, turtleHeading)

		moveForward()

		-- Return to base
		if turtlePosition.z >= endCoords.z then
			if os.clock() < sweepTimer then
				local wait = sweepTimer	- os.clock()
				print('Waiting', wait, 'seconds before final sweep...')
				os.sleep(wait)
			end
			sweepSaplings(vector.new(-1,1,0), 'W', false)
			print('Returning to base.')
			moveToOrigin()

			-- Refuel if necessary
			if turtle.getFuelLevel() <= 1000 then
				local slot = findEmptySlot()
				local fuelItemsUsed = 0
				while turtle.getFuelLevel() <= 4500 do
					turtle.select(slot)
					turtle.suck(1)
					turtle.refuel()
					local fuelItemsUsed = fuelItemsUsed + 1
				end
				print('Turtle refueled with', fuelItemsUsed, 'fuel items.')
			end

			-- Deposit harvest in chest
			moveToPosition(vector.new(-5,0,-2), {'Y','Z','X'})
			dumpAllItemsDown()
			moveToPosition(vector.new(0,0,0), {'X','Z','Y'})
			faceHeading('W')

			print('Harvest complete.')
			return

		-- Alternating turn direction
		elseif turnDirection == 'L' then
			turnLeft()
			moveForward(2)
			turnLeft()
			turnDirection = 'R'
		elseif turnDirection == 'R' then
			turnRight()
			moveForward(2)
			turnRight()
			turnDirection = 'L'
		end
	end
end

main()
