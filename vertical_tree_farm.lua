-- Operate a vertical tree farm

require('/api/INS')

NUMBER_OF_TREES = 12
SPACING_BETWEEN = 8

SLOTS = 16

-- Actual starting position
turtlePosition = vector.new(-58, -56, 356)
turtleHeading = 'E'

function digMoveForward()
	local steps = steps or 1
	for i = 1, steps do
		if turtle.detect() then turtle.dig() end
		moveForward()
	end
end

function digMoveUp(steps)
	local steps = steps or 1
	for i = 1, steps do
		if turtle.detectUp() then turtle.digUp() end
		moveUp()
	end
end

function goAroundDirt()
	-- Moves around a dirt block directly above
	digMoveForward()
	digMoveUp(2)
	turnLeft(2)
	digMoveForward()
end

function itemIsSapling()
	local itemDetail = turtle.getItemDetail()
	if string.find(itemDetail.name, 'sapling') then
		return true
	else
		return false
	end
end

function findSaplingSlot()
	-- Finds saplings in the inventory. Stores slot# in global _G.SAPLING_SLOT
	for slot = 1, SLOTS do
		turtle.select(slot)
		if turtle.getItemCount() > 0 and itemIsSapling() then
			_G.SAPLING_SLOT = slot -- Global
			return slot
		end
	end
end

function plantTree()
	turtle.select(_G.SAPLING_SLOT)
	if turtle.getItemCount() <= 0 or not itemIsSapling() then
		turtle.select(findSaplingSlot())
	end
	turtle.placeDown()
end

function travelThroughTunnel(direction)
	-- Travel through the turtle tunnel up or down.
	-- Start just outside, facing the tunnel. Ends just outside, facing away.
	moveForward(2)
	turnRight(2)
	if direction == 'down' then
		moveDown(3)
	elseif direction == 'up' then
		moveUp(3)
	end
	moveForward(2)
end

-- Assume starting in control room at standard spot facing East

-- Refuel if necessary
if turtle.getFuelLevel() < 500 then
	moveDown(3)
	faceHeading('S')
	turtle.select(SLOTS)
	while turtle.getFuelLevel() < 1000 do
		turtle.suck(1)
		turtle.refuel()
	end
	moveUp(3)
end

assert(findSaplingSlot() ~= nil, 'No saplings detected')

-- Travel to base of first tree
moveUp()
faceHeading('W')
travelThroughTunnel('up')
moveToPosition(vector.new(-56, -50, 356), true, {'y', 'x', 'z'})

-- Begin harvest
for tree = 1, NUMBER_OF_TREES do
	digMoveUp()
	plantTree()
	digMoveUp(SPACING_BETWEEN-2)
	if tree < NUMBER_OF_TREES then goAroundDirt() end
end

-- Return to mouth of turtle tunnel
moveToPosition(vector.new(-58, -52, 356), true, {'x', 'z', 'y'})
travelThroughTunnel('down')

-- In control room:
moveDown()
turnLeft()
-- Put all items into main chest
for slot = 1, SLOTS do
	turtle.select(slot)
	turtle.drop()
end
turnRight()

