-- Drill a vertical tunnel according to a cross section read from a .xsec file.
-- Available symbols are listed in the FILE_SYMBOL table.

require('/api/INS')
-- require('/api/file_handling')

FILE_SYMBOL = {
	DIG    = 'X',
	FILL   = 'F',
	TURTLE = 'O',
	LEAVE  = ' ',
}

function readXsec(filename)
	local rows = {}

	for line in io.lines(filename) do
		local row = {}
		for i = 1, #line do
			row[#row + 1] = line:sub(i, 1)
		end
		rows[#rows + 1] = row
	end

	return crossSection
end

function widestXLength(crossSection)
	local length = 0
	for row in crossSection do
		local rowLength = #row
		if (rowLength > length) then
			length = rowLength
		end
	end
	return length
end

function widestYLength(crossSection)
	return #crossSection
end

function turtleStartingLocation(crossSection, depth)
	-- Return the turtle's starting coords
	for rowIdx, row in crossSection do
		for colIdx, col in row do
			if (col == FILE_SYMBOL.TURTLE) then
				return vector.new(rowIdx, 0-depth, colIdx)
			end
		end
	end
end

-- Startup

local sectionFilename = arg[1]

crossSection = readXsec(sectionFilename)

X_MAX = widestXLength(crossSection)
Y_MAX = widestYLength(crossSection)
Z_MAX = 0 - arg[2]

turtlePosition:add(turtleStartingLocation(crossSection))



